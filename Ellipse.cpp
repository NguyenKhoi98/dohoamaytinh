#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    SDL_RenderDrawPoint(ren, xc+x,yc+y);
    SDL_RenderDrawPoint(ren, xc-x,yc+y);
    SDL_RenderDrawPoint(ren, xc-x,yc-y);
    SDL_RenderDrawPoint(ren, xc+x,yc-y);

}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    float p,a2,b2;
    int x,y;
    a2 = a * a;
    b2 = b * b;
    x = 0;
    y = b;
    float ba = b2/a2;
    float ab = a2/b2;

    p = 2*(ba-(2*b)+1);

    // Area 1
    while(ba*x <= y)
    {
        Draw4Points(xc,yc,x,y,ren);
        if(p<0)
        {
            p += 2 * (ba * (2 * x + 3));
        }
        else{
            p -= 4 * y + 2 * (ba * (2 * x + 3));
            y--;
        }
        x++;
    }

    // Area 2
    y = 0;
    x = a;
    p = 2 * (ab - 2 * a + 1);
    while((ab * y) <= x)
    {
        Draw4Points(xc,yc,x,y,ren);
        if(p<0)
        {
            p += 2 * (ab * (2 * y + 3));
        }
        else
        {
            p -= 4*x + 2*(ab*(2*y+3));
            x--;
        }
        y++;
    }

}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    int x = 0;
    int y = b;
    int a2 = a*a;
    int b2 = b*b;
    int fx = 0;
    int fy = 2 * a2 * y;

    Draw4Points(xc, yc, x, y, ren);

    int p = int(b2 -(a2*b) + (a2/4) + 0.5);

    // Area 1
    while(fx<fy)
    {
        x++;
        fx += 2*b2;
        if(p<0)
        {
            p += b2*(2*x + 3);
        }
        else
        {
            y--;
            p += b2*(2*x +3) + a2*(2- 2*y);
            fy -= 2*a2;
        }
        Draw4Points(xc, yc, x, y, ren);
    }
    p = int(b2*(x +0.5)*(x +0.5) + a2*(y-1)*(y-1) - a2*b2 + 0.5);

    // Area 2
    while(y>0)
    {
        y--;
        fy -= 2*a2;
        if(p >=0)
        {
            p += a2*(3-2*y);
        }
        else
        {
            x++;
            fx += 2*b2;
            p += b2*(2*x +2) +a2*(3- 2*y);
        }
        Draw4Points(xc, yc, x, y, ren);
    }
}
